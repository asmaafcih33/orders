const constants= require('../../common/constant/business.constant')


module.exports.findOrdersByStatusIdAndAgentIdScript=(bind) =>{
  
  let script=`SELECT o.STATUS_ID , o.ORDER_NUMBER as orderNumber ,o.ORDER_DATE as orderDate,
              o.REJECTION_REASON as rejectionReason ,
              a.NAME_ARABIC as agent_nameArabic ,a.NAME_ENGLISH as agent_nameEnglish,
              l.NAME_ARABIC as orderReasonLookup_nameArabic
              FROM DCA_ORDER o,DCA_AGENT a,DCA_SYSTEM_LOOKUPS l
              WHERE a.AGENT_ID=o.AGENT_ID and o.REASON_CODE_ID =l.LOOKUP_ID 
              AND l.LOOKUP_TYPE_ID =14
              AND o.STATUS_ID in (${bind.statusId}) `;
                   if(bind.agentId !=undefined){
                      script +=`AND o.AGENT_ID = ${bind.agentId} `;
                   }
                   script +=`ORDER BY  ${bind.property} ${bind.direction}`;
                  
  return script;            
};
module.exports.findByStatusIdAndAgentIdPageScript=(bind) =>{
  let script=`SELECT outer.*
              FROM (SELECT ROWNUM rn, inner.*
                FROM (SELECT * 
                      FROM DCA_ORDER o INNER JOIN DCA_AGENT a 
                        ON a.AGENT_ID=o.AGENT_ID    
                        WHERE o.STATUS_ID = ${bind.statusId} AND o.AGENT_ID = ${bind.agentId}
                        ORDER BY  ${bind.orderBy} ${ordering}
                        ) inner) outer
              WHERE outer.rn >= ${bind.offest} AND outer.rn <= ${bind.limit}`;
  return script;            
};
module.exports.getOrdersByOrderNumberScript=(bind) =>{
  
  let script=`SELECT o.ORDER_ID ,o.STATUS_ID ,o.ORDER_NUMBER as orderNumber ,
              o.ORDER_DATE as orderDate,
              l.NAME_ARABIC as orderReasonLookup_nameArabic
              FROM DCA_ORDER o , DCA_SYSTEM_LOOKUPS l
              WHERE  o.REASON_CODE_ID =l.LOOKUP_ID and
              o.ORDER_NUMBER = ${bind.orderNumber} AND l.LOOKUP_TYPE_ID =14`; 

  return script;            
};
module.exports.getProductByOrderIdScript=(bind) =>{
  
  let script=`SELECT p.PRODUCT_ID as PRODUCT_ID ,p.name_english as product_nameEnglish
              ,p.name_arabic as product_nameArabic,
               order_pro.QUANTITY as quantity
              FROM DCA_ORDER_PRODUCTS order_pro INNER JOIN  DCA_ORDER o
              ON order_pro.ORDER_ID=o.ORDER_ID INNER JOIN DCA_PRODUCTS p
              ON  order_pro.PRODUCT_ID=p.PRODUCT_ID 
              WHERE o.ORDER_ID = ${bind.ORDER_ID}`; 
  return script;            
};
module.exports.getApprovedQuantityScript=(bind) =>{  
  let script=`select p.QUANTITY as quantityApproved
              from DCA_TRANSACTIONS t left outer join 
              DCA_TRANSACTION_PRODUCTS p on t.TRANSACTION_ID=p.TRANSACTION_ID 
              where t.ORDER_ID= ${bind.orderId} and p.PRODUCT_ID=${bind.productId}`; 

  return script;            
};
module.exports.getOrdersByIdScript=(bind) =>{  
  let script=`SELECT o.ORDER_ID ,o.SUBMISSION_TIME as SUBMISSION_TIME ,o.ORDER_NUMBER,o.STATUS_ID
              FROM DCA_ORDER o 
              where o.STATUS_ID= ${constants.ORDER_STATUS_PENDING} 
              AND o.ORDER_ID in(${bind.orderIds})`; 
  return script;            
};
module.exports.updateOrderStatusScript=(bind) =>{ 
  let script=`UPDATE  DCA_ORDER o 
              SET o.STATUS_ID =${constants.ORDER_STATUS_DRAFT},
              o.LOGIN_ID = ${bind.loginId}
              WHERE  o.ORDER_ID in (${bind.orderIds})`; 

  return script;            
};
module.exports.submitOrderScript=(bind) =>{ 
  let script=`UPDATE DCA_ORDER o 
              SET o.STATUS_ID =${constants.ORDER_STATUS_PENDING},
              o.LOGIN_ID = ${bind.loginId},
              o.SUBMISSION_TIME=CURRENT_TIMESTAMP
              WHERE o.ORDER_ID =${bind.orderId}
              AND o.STATUS_ID= ${constants.ORDER_STATUS_DRAFT}`; 
  return script;            
};
module.exports.deleteOrderProductScript=(bind) =>{ 
  let script=`DELETE FROM DCA_TRANSACTIONS trans
              WHERE trans.ORDER_ID =${bind.orderId}`; 
  return script;            
};
module.exports.deleteOrderTransactionScript=(bind) =>{ 
  let script=`DELETE FROM DCA_ORDER_PRODUCTS o
              WHERE o.ORDER_ID =${bind.orderId}`; 
  return script;            
};
module.exports.deleteOrderScript=(bind) =>{ 
  let script=`DELETE FROM DCA_ORDER o
              WHERE o.ORDER_ID =${bind.orderId}`; 
return script;            
};
module.exports.findOrderProductByOrderIdAndProductIdScript=(bind) =>{ 
  let script=`SELECT o.quantity FROM DCA_ORDER_PRODUCTS o
              WHERE o.ORDER_ID =${bind.orderId} AND o.PRODUCT_ID=${bind.productId}`; 
return script;            
};
module.exports.updateOrderProductByOrderIdAndProductIdScript=(bind) =>{ 
  let script=`UPDATE DCA_ORDER_PRODUCTS o
              SET o.LOGIN_ID=${bind.loginId},o.quantity= ${bind.quantity}
              WHERE o.ORDER_ID =${bind.orderId} AND o.PRODUCT_ID=${bind.productId}
              returning ORDER_PRODUCT_ID
              into :id`; 
return script;            
};
module.exports.createOrderProductScript=(bind) =>{ 
  let script=`INSERT INTO DCA_ORDER_PRODUCTS 
              (ORDER_PRODUCT_ID,quantity,ORDER_ID,PRODUCT_ID ,LOGIN_ID)
              VALUES
              (DCA_ORDER_PRODUCTS_SEQ.NEXTVAL,${bind.quantity},
              ${bind.orderId},${bind.productId},${bind.loginId})
              returning ORDER_PRODUCT_ID
              into :id`; 
return script;            
};
module.exports.createOrderScript=(bind) =>{ 
  let script=`INSERT INTO DCA_ORDER 
              (ORDER_ID,LOGIN_ID,ORDER_DATE,ORDER_NUMBER,STATUS_ID, AGENT_ID,REASON_CODE_ID )
               VALUES
              (DCA_ORDER_SEQ.NEXTVAL,${bind.loginId},CURRENT_TIMESTAMP, DCA_ORDER_NUMBER_SEQ.NEXTVAL,
              ${bind.statusId},${bind.agentId},${bind.reasonCodeId})
              returning ORDER_ID
              into :id` 
 return script;            
};
module.exports.deleteProductbyOrderProductIdScript=(bind) =>{ 
  let script=`DELETE FROM DCA_ORDER_PRODUCTS o
              WHERE o.ORDER_PRODUCT_ID =${bind.orderProductId}`; 
return script;            
};
