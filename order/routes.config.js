const OrderController = require('../order/controller/order.controller');


exports.routesConfig = function (app,uri) {
    app.get(`/${uri}/draftOrders`, [
         OrderController.getDraftOrders
    ]);
    app.get(`/${uri}/pendingOrders`, [
        OrderController.getPendingOrders
    ]);    
     app.get(`/${uri}/rejectedOrders`, [
        OrderController.getRejectedOrders
    ]);
    app.get(`/${uri}/approvedOrders`, [
        OrderController.getApprovedOrders
    ]);
    app.get(`/${uri}/openOrder`, [
        OrderController.openOrder
    ]);
    app.post(`/${uri}/cancelPendingOrder`, [
        OrderController.cancelPendingOrder
    ]);
    app.post(`/${uri}/submitOrder`, [
        OrderController.submitOrder
    ]);
    app.get(`/${uri}/deleteOrder`, [
        OrderController.deleteOrder
    ]);
    app.post(`/${uri}/addOrderProduct`, [
        OrderController.addOrderProduct
    ]);
    app.post(`/${uri}/addOrder`, [
        OrderController.addOrder
    ]);
    app.get(`/${uri}/deleteProduct`, [
        OrderController.deleteProduct
    ]);
    
};