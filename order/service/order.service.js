const orderDao = require('../../order/dao/order.dao');
const logger=require('../../common/util/logger.util');
const constants= require('../../common/constant/business.constant')
const AppError = require('../../common/exception/app.exception');


exports.getOrdersByStatusAndAgentId = (query) => {
  return new Promise(async (resolve, reject) => {
      logger.info(`Order.service : getOrdersByStatusAndAgentId : Started`);
      if(query.sort!=undefined){
       if(query.statusId == constants.ORDER_STATUS_REJECTED){
          query= mapRejectedPageSorting(query);
        }else if(query.statusId.includes(`,${constants.ORDER_STATUS_APPROVED4}`)){
          query= mapApprovedPageSorting(query);
        }else{
          /* cas pending,draft orders */
          query= mapPageSorting(query);
        }
        }else{
        query.property='ORDER_DATE';
        query.direction='DESC';
      }

    try{
      let orders= await orderDao.findByStatusIdAndAgentId(query);
      logger.info(`Order.service : getOrdersByStatusAndAgentId : Ended`);
     resolve(orders); 
  }
  catch(err){
     reject(err)     
  }  
});}

function mapApprovedPageSorting( query ) {
 
  logger.info(`Order.service : mapApprovedPageSorting : Started`);
   sort=query.sort.split(",");
      /* property*/
      if(sort[0]=='0'){
        query.property='o.ORDER_NUMBER';
      }else if(sort[0]=='1'){
        query.property='o.ORDER_DATE';
      }else if(sort[0]=='2') {
        if(query.agentId!= undefined){
          query.property='a.NAME_ARABIC';
        }else{
            query.property='a.NAME_ENGLISH';
        }
      }else if(sort[0]=='3'){
        query.property='l.NAME_ARABIC';
      }else{
        query.property='o.ORDER_DATE';
      }
  /* *direction*/
if(sort[1] !=undefined && (sort[1].toUpperCase()=='DESC' 
  || sort[1].toUpperCase()=='ASC')){
    query.direction=sort[1].toUpperCase();  
  }else{
      query.direction='DESC';
  }
  logger.info(`Order.service : mapApprovedPageSorting : Ended`);
  return query;
};

function mapRejectedPageSorting( query ) {

  logger.info(`Order.service : mapRejectedPageSorting : Started`);
  sort=query.sort.split(",");
    /* property*/
    if(sort[0]=='0'){
      query.property='o.ORDER_NUMBER';
    }else if(sort[0]=='1'){
      query.property='o.ORDER_DATE';
    }else if(sort[0]=='2') {
      if(query.agentId!= undefined){
        query.property='a.NAME_ARABIC';
      }else{
          query.property='a.NAME_ENGLISH';
      }
    }else if(sort[0]=='3'){
      query.property='o.REJECTION_REASON';
    }else{
      query.property='o.ORDER_DATE';
    }
/* *direction*/
if(sort[1] !=undefined && (sort[1].toUpperCase()=='DESC'
   || sort[1].toUpperCase()=='ASC')){
  query.direction=sort[1].toUpperCase();  
}else{
    query.direction='DESC';
}
logger.info(`Order.service : mapRejectedPageSorting : Ended`);
return query;
};

function mapPageSorting( query ) {
  logger.info(`Order.service : mapPageSorting : Started`);
  sort=query.sort.split(",");
    /* property*/
    if(sort[0]=='1'){
      query.property='o.ORDER_NUMBER';
    }else if(sort[0]=='2'){
      query.property='o.ORDER_DATE';
    }else if(sort[0]=='3') {
      if(query.agentId!= undefined){
        query.property='l.NAME_ARABIC';
      }else{
          query.property='l.NAME_ENGLISH';
      }
    }else{
      query.property='o.ORDER_DATE';
    }
/* *direction*/
if(sort[1] !=undefined && (sort[1].toUpperCase()=='DESC' 
  || sort[1].toUpperCase()=='ASC')){
  query.direction=sort[1].toUpperCase();  
}else{
    query.direction='DESC';
}
logger.info(`Order.service : mapPageSorting : Ended`);
return query;
};

exports.openOrder =(query) => {
  return new Promise(async (resolve, reject) => {
  try{
    logger.info(`Order.service : openOrder : Started`);
    let orders= await orderDao.getOrdersByOrderNumber(query);
    if(orders.length !=0){
      let order_id=orders[0].ORDER_ID;
      let products= await orderDao.getProductByOrderId({ORDER_ID:order_id}); 
      if(orders[0].STATUS_ID == constants.ORDER_STATUS_APPROVED1){
      for(var i = 0; i < products.length;i++){
          query.orderId = orders[0].ORDER_ID;
          query.productId = products[i].PRODUCT_ID;
          let approvedQuantity= await orderDao.getApprovedQuantity(query);
          products[0].approvedQuantity=approvedQuantity;
        }
     }
      orders[0].products=products;
      resolve(orders); 
    }else{
      res='Order not found';
      resolve(res)
    }
    logger.info(`Order.service : openOrder : Ended`);
  }
  catch(err){
     reject(err)     
  }  
});
}

exports.cancelPendingOrder =(query) => {
  return new Promise(async (resolve, reject) => {
  try{
    logger.info(`Order.service : cancelPendingOrder : Started`);
    let orders= await orderDao.getOrdersById(query);
    if(orders.length !=0){
      checkOrderListAvailableForCancelation(orders); 
      let rowsAffected= await orderDao.updateOrderStatusAndLoginId(query);  
      if(rowsAffected !=0){
        res=`Orders Updated Successfully`; 
     }else{
       res=`Orders not Updated `; 
     }
  }else{
    res=`Orders not found`;
  }
  logger.info(`Order.service : cancelPendingOrder : Ended`);
  resolve(res);
}
  catch(err){
     reject(err)     
  }});
}

function checkOrderListAvailableForCancelation(orders){
  var invalidOrders = null;
  var calendar = new Date();
  calendar.setHours(calendar.getHours() - 1)
  
  for(let index=0 ; index < orders.length ; index++){
    if(orders[index].SUBMISSION_TIME != null){
      let submationDate=new Date(orders[index].SUBMISSION_TIME) ;
      if (calendar.getTime()>submationDate.getTime()){
          invalidOrders +=orders[index].ORDER_NUMBER;
          if(index < orders.length-1){
            invalidOrders+=",";
        }
      }
    } 
  }
  if(invalidOrders != null){
    throw (new AppError('Order.service','cancelPendingOrder'
             ,constants.INTERNAL_SERVER_ERROR_CODE
             ,constants.INTERNAL_SERVER_ERROR_KEY
            ,`Invalid Orders: ${invalidOrders} order exceed time`));
  }
}

exports.submitOrder =(query) => {
  return new Promise(async (resolve, reject) => {
  try{
    logger.info(`Order.service : submitOrder : Started`);
      let rowsAffected= await orderDao.submitOrder(query);  
      if(rowsAffected !=0){
        res=`Orders Sumitted Successfully`; 
     }else{
        res=`Orders not Found `; 
     }
     logger.info(`Order.service : submitOrder : Ended`);
     resolve(res)
    }
  catch(err){
     reject(err)     
  }});
}

exports.deleteOrder =(query) => {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.service : deleteOrder : Started`);
      let res;
      let rowsAffected= await orderDao.deleteOrder(query);  
      if(rowsAffected !=0){
        res=`Orders deleted Successfully`; 
     }else{
      res=`Order not Found `; 
     }
     logger.info(`Order.service : deleteOrder : Ended`);
     resolve(res);
    }
  catch(err){
     reject(err)     
  }});
}

exports.addOrderProduct =(query) => {
  return new Promise(async (resolve, reject) => {
  try{
    /*find by productId,orderId 
     *If Exists Update(quantity and loginId)
     * else add 
     */
    let id;
    let res;
    logger.info(`Order.service : addOrderProduct : Started`);
      let orderProduct= await orderDao.findOrderProductByOrderIdAndProductId(query);  
      if(orderProduct!=undefined &&orderProduct[0]!=undefined 
          && orderProduct[0].QUANTITY !=undefined){
      /**update quantity and loginId */
        query.quantity+=orderProduct[0].QUANTITY;
        id= await orderDao.updateOrderProductByOrderIdAndProductId(query);  
     }else{
       /**create productOrder */
       id= await orderDao.createOrderProduct(query);  
      }
      if(id!=undefined && id!=0){
         res=`prodcut with ${id} Added/Updated Successfully`; 
     }else{
         res=`can't add/Update Prodcut`; 
      }
      logger.info(`Order.service : addOrderProduct : Ended`);
      resolve(res);
   }catch(err){
     reject(err)     
  }});
}

exports.addOrder=(query) => {
  return new Promise(async (resolve, reject) => {
  try{
     let id;
     logger.info(`Order.service : addOrder : Started`);
      /**create productOrder */
     query.statusId=constants.ORDER_STATUS_DRAFT;
     query.reasonCodeId=constants.SYSTEM_LOOKUP_ORDER_REASON_CODE_AGENT_ORDER;
     id= await orderDao.addOrder(query);  
     let res;
     if(id !=undefined && id!=0){
         res=`Order with Id ${id} Added Successfully`; 
     }else{
         res=`can't add Order`; 
      }
      logger.info(`Order.service : addOrder : Ended`);
      resolve(res)
    }catch(err){
     reject(err)     
  }});
}

exports.deleteOrderProduct=(query) => {
  return new Promise(async (resolve, reject) => {
  try{
     let rowsAffected;
     logger.info(`Order.service : deleteOrderProduct : Started`);
      /**create productOrder */
     rowsAffected= await orderDao.deleteProductByOrderProductId(query);  
     let res;
     if(rowsAffected!=0){
         res=`product deleted Successfully`; 
     }else{
         res=`product not found `; 
      }
      logger.info(`Order.service : deleteOrderProduct : Ended`);
      resolve(res); 
   }catch(err){
     reject(err)     
  }});
}