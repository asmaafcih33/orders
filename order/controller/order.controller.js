const orderService=require('../../order/service/order.service');
const logger=require('../../common/util/logger.util');
const constants= require('../../common/constant/business.constant')
const _ = require('lodash');
const AppError = require('../../common/exception/app.exception');


exports.getRejectedOrders = (req, res, next) => {
 
    logger.info(`Order.controller : getRejectedOrders : Started `);
    req.query.statusId = constants.ORDER_STATUS_REJECTED;
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:constants.NO_ERRORS_CODE,
                    key:constants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
            logger.info(`Order.controller : getRejectedOrders : End sucessfully `);
        })
        .catch(error =>{
            next(error);
         });
        
};

exports.getPendingOrders = (req, res, next) => {
    
    logger.info(`Order.controller : getPendingOrders : Started `);
    req.query.statusId= constants.ORDER_STATUS_PENDING;
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:constants.NO_ERRORS_CODE,
                    key:constants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            next(error);
         });
        
};

exports.getDraftOrders = (req, res, next) => {
    
    logger.info(`Order.controller : getDraftOrders : Started `);
    req.query.statusId=constants.ORDER_STATUS_DRAFT
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:constants.NO_ERRORS_CODE,
                    key:constants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            next(error);
        });
        
};

exports.getApprovedOrders = (req, res, next) => {
    
    logger.info(`Order.controller : getApprovedOrders : Started  `);
    req.query.statusId=`${constants.ORDER_STATUS_APPROVED1},
                        ${constants.ORDER_STATUS_APPROVED2},
                        ${constants.ORDER_STATUS_APPROVED3},
                        ${constants.ORDER_STATUS_APPROVED4}`
    req.query.statusId= req.query.statusId .replace(/ /g,"").replace(/\n/g,"");
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:constants.NO_ERRORS_CODE,
                    key:constants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            next(error);
         });
        
};

exports.openOrder = (req, res, next) => {
  
    logger.info(`Order.service : openOrder : Started`);
    if(req.query.orderNumber==undefined || isNaN(req.query.orderNumber)){
        throw new AppError('order.Controller','openOrder',
                          constants.INVALID_REQUIRED_FIELD_CODE,
                          constants.INVALID_REQUIRED_FIELD_KEY
                          );
      }
    orderService.openOrder(req.query)
        .then((result)=>{
            logger.info(`Order.controller : openOrder : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:constants.NO_ERRORS_CODE,
                    key:constants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            next(error);
         });
        
};


exports.cancelPendingOrder = (req, res, next) => {
   
    logger.info(`Order.controller : cancelPendingOrder : Started `);
      if(req.body ==undefined || req.body.orderIds ==undefined || 
        !_.isArray(req.body.orderIds) || req.body.loginId ==undefined ||
        isNaN (req.body.loginId )|| req.body.orderIds.some(isNaN) ){

            throw new AppError('order.Controller','cancelPendingOrder',
            constants.INVALID_REQUIRED_FIELD_CODE,
            constants.INVALID_REQUIRED_FIELD_KEY,
            );
        }
        
      orderService.cancelPendingOrder(req.body)
          .then((result)=>{
              logger.info(`Order.controller : cancelPendingOrder : End sucessfully `);
              res.setHeader("Content-Type", "application/json"); 
              result={
                      code:constants.NO_ERRORS_CODE,
                      key:constants.NO_ERRORS_KEY,
                      response:result
                  }
              res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
          })
          .catch(error =>{
            next(error);
           });
       
  };
  
  exports.submitOrder = (req, res, next) => {
   
    logger.info(`Order.controller : submitOrder : Started `);
      if(req.body ==undefined || req.body.orderId ==undefined 
        || isNaN(req.body.orderId) ) {
         
            throw new AppError('order.Controller','submitOrder',
            constants.INVALID_REQUIRED_FIELD_CODE,
            constants.INVALID_REQUIRED_FIELD_KEY,
            );
         }
      orderService.submitOrder(req.body)
          .then((result)=>{
              logger.info(`Order.controller : submitOrder : End sucessfully `);
              res.setHeader("Content-Type", "application/json"); 
              result={
                      code:constants.NO_ERRORS_CODE,
                      key:constants.NO_ERRORS_KEY,
                      response:result
                  }
              res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
          })
          .catch(error =>{
              next(error);
           });
          
  };
  
  exports.deleteOrder = (req, res, next) => {
     
      logger.info(`Order.controller : deleteOrder : Started `);
      if(req.query ==undefined || req.query.orderId ==undefined 
        || isNaN(req.query.orderId) ) {

            throw new AppError('order.Controller','deleteOrder',
            constants.INVALID_REQUIRED_FIELD_CODE,
            constants.INVALID_REQUIRED_FIELD_KEY,
            );
        }
      orderService.deleteOrder(req.query)
          .then((result)=>{
              logger.info(`Order.controller : deleteOrder : End sucessfully `);
              res.setHeader("Content-Type", "application/json"); 
              result={
                      code:constants.NO_ERRORS_CODE,
                      key:constants.NO_ERRORS_KEY,
                      response:result
                  }
              res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
          })
          .catch(error =>{
              next(error);
           });
   };
  
  exports.addOrderProduct = (req, res, next) => {
    
      logger.info(`Order.controller : addOrderProduct : Started `);
      if(req.body ==undefined || req.body.orderId ==undefined 
            || isNaN(req.body.orderId)   || req.body.quantity ==undefined 
            || isNaN(req.body.quantity)  || req.body.loginId ==undefined 
            || isNaN(req.body.loginId)   || req.body.productId ==undefined 
            || isNaN(req.body.productId)) {

                throw new AppError('order.Controller','addOrderProduct',
                constants.INVALID_REQUIRED_FIELD_CODE,
                constants.INVALID_REQUIRED_FIELD_KEY,
                ); 
        }
      orderService.addOrderProduct(req.body)
          .then((result)=>{
              logger.info(`Order.controller : addOrderProduct : End sucessfully `);
              res.setHeader("Content-Type", "application/json"); 
              result={
                      code:constants.NO_ERRORS_CODE,
                      key:constants.NO_ERRORS_KEY,
                      response:result
                  }
              res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
          })
          .catch(error =>{
              next(error);
           });     
  };

  exports.addOrder = (req, res, next) => {
    
      logger.info(`Order.controller : addOrder : Started `);
      if(req.body ==undefined || req.body.agentId ==undefined 
            || isNaN(req.body.agentId)   || req.body.loginId ==undefined 
            || isNaN(req.body.loginId) ) {

                throw new AppError('order.Controller','addOrder',
                constants.INVALID_REQUIRED_FIELD_CODE,
                constants.INVALID_REQUIRED_FIELD_KEY,
                ); 
        }
      orderService.addOrder(req.body)
          .then((result)=>{
              logger.info(`Order.controller : addOrder : End sucessfully `);
              res.setHeader("Content-Type", "application/json"); 
              result={
                      code:constants.NO_ERRORS_CODE,
                      key:constants.NO_ERRORS_KEY,
                      response:result
                  }
              res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
          })
          .catch(error =>{
              next(error);
            });
  };


  exports.deleteProduct = (req, res, next) => {
    logger.info(`Order.controller : deleteProduct : Started `);
      if(req.query ==undefined || req.query.orderProductId ==undefined 
            || isNaN(req.query.orderProductId)) {
                throw new AppError('order.Controller','deleteProduct',
                constants.INVALID_REQUIRED_FIELD_CODE,
                constants.INVALID_REQUIRED_FIELD_KEY,
                ); 
        }
      orderService.deleteOrderProduct(req.query)
          .then((result)=>{
              logger.info(`Order.controller : deleteProduct : End sucessfully `);
              res.setHeader("Content-Type", "application/json"); 
              result={
                      code:constants.NO_ERRORS_CODE,
                      key:constants.NO_ERRORS_KEY,
                      response:result
                  }
              res.status(constants.NO_ERRORS_CODE).send(JSON.stringify(result));
          })
          .catch(error =>{
              next(error);
           });
        
  };

  