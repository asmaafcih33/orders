module.exports = {
  port: process.env.HTTP_PORT || 8080|| 3000 ,
  host: process.env.HTTP_HOST || '0.0.0.0' ||'localhost'||'10.100.255.138'
};
