const dbConfig = require('../config/database.config.js');
const oracledb = require('oracledb');

async function initialize() {
    await oracledb.createPool(dbConfig.dcaPool);
  }

module.exports.initialize = initialize;

async function close() {
  await oracledb.getPool().close();
}

module.exports.close = close;
